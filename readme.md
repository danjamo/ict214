# Quick Start
This will guide you to the installation process of this project

## Download Only
Go to the upper right corner of the screen and find the `cloud icon` beside find file button
Click and choose a file format.

> To clone this project, refer to the instructions below 

## Installation Requirements
1. Install these softwares:
    - **[Git Bash](https://gitforwindows.org/)**
    - **[Node JS](https://nodejs.org/en/)**
2. Go to the selected folder/directory and open it with Git Bash and write the command
```
$ git clone https://gitlab.com/danjamo/ict214.git project-name
$ npm install
```

# Development
Open Git Bash and run this command:
```
$ gulp watch
```

// gulpfile.js
var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();

// Put this after including our dependencies
var paths = {
    styles: {
        // By using styles/**/*.sass we're telling gulp to check all folders for any sass file
        src: "src/scss/**/*.scss",
        // Compiled files will end up in whichever folder it's found in (partials are not compiled)
        dest: "src/css"
    }
 
    // Easily add additional paths
    // ,html: {
    //  src: '...',
    //  dest: '...'
    // }
};

// Additional paths
var sassPaths = [
    'node_modules/bootstrap/scss',
    // 'node_modules/@fortawesome/fontawesome-free/scss',
    // 'node_modules/@fortawesome/fontawesome-free/scss/solid.scss',
    // 'node_modules/@fortawesome/fontawesome-free/scss/regular.scss',
    // 'node_modules/@fortawesome/fontawesome-free/scss/brands'

];

// Define tasks after requiring dependencies
function style() {
    // Where should gulp look for the sass files?
    // My .sass/.scss files are stored in docs/scss folder
    return (
    	gulp
	        .src(paths.styles.src)
	        .pipe(sass({
	        	includePaths: sassPaths,
	        	outputStyle: 'compressed'
	        }))
	        .on("error", sass.logError)
	        .pipe(gulp.dest(paths.styles.dest))

	        // Add browsersync stream pipe after compilation
            .pipe(browserSync.stream())
    );
}

function watch() {
    browserSync.init({
        injectChanges: true,
        // You can tell browserSync to use this directory and serve it as a mini-server
        server: {
            // baseDir: ["src/", "./", "src/scripts/"],
            baseDir: "src",
            directory: true,
            index: "index.html"
        }
        // If you are already serving your website locally using something like apache
        // You can use the proxy setting to proxy that instead
        // proxy: "yourlocal.dev"
    });

    gulp.watch("src/scss/**/*.scss", style);
    gulp.watch(['src/*.html', 'src/scripts/*.js']).on('change', browserSync.reload);
    // gulp.watch('src/*.php').on('change', browserSync.reload);
} 
// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style
exports.style = style;

// Don't forget to expose the task!
exports.watch = watch;
function enterClear() {
    var text = $("#chat-content").text();
    $(".chat-preview").append("<div class='chat-preview-sen-msg media p-2 ml-auto'><div class='media-body'><div class='bg-primary text-white rounded p-2 my-1'>" + text + "</div></div></div>");
    $("#chat-content").empty();
}

$(document).ready(function (){
    $("#panel-toggler").on("click", function () {
        $(".panel-box").toggleClass("panel-box-show");
        // alert('clicked');
    })

    $('[data-toggle="tooltip"]').tooltip();

    $("#btn-send").click(enterClear);

    $("#chat-content").on('keypress', function (e) {
        if (e.keyCode == 13 && !e.shiftKey) {
            enterClear();
        }
    });
})

